package jediss.ha666.com;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import redis.clients.jedis.BinaryJedis;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

public class testJediss {

	private static Jedis jedis;
	private static BinaryJedis binaryJedis;
	private static Pipeline pipeline;
	private static Logger logger = Logger.getLogger(testJediss.class);
	
	public testJediss() {
		jedis = redisAPI.getJedis();
		binaryJedis=new BinaryJedis("127.0.0.1", 26125, 5000);
		pipeline = jedis.pipelined();
	}
	
	public static void main(String[] args) {
		testJediss redis = new testJediss();
		if (pipeline == null) {
			logger.error("连接redis失败！");
			return;
		}
		logger.info("连接redis成功！");
		//testStringGetAndSet();
		GetBinary();
	}
	
	private static void GetBinary() {
		try {
			byte[] bytes=binaryJedis.get("ha666.trade.78943561326512345".getBytes());
			System.out.println(bytes.length);
			TradeProtobuf.tradeBuf msg = TradeProtobuf.tradeBuf.parseFrom(bytes);  
	        System.out.println(msg);
	        System.out.println("Tid：\t"+msg.getTid());
	        System.out.println("Title：\t"+msg.getTitle());
	        System.out.println("Status：\t"+msg.getStatus());
	        System.out.println("Created：\t"+msg.getCreatedBytes());
	        System.out.println("Modified：\t"+msg.getModifiedBytes());
	        System.out.println("SellerNick：\t"+msg.getSellerNick());
	        System.out.println("BuyerNick：\t"+msg.getBuyerNick());
		} catch (Exception e) {
			System.err.print(e);
		}
	}
	
	private static void testStringGetAndSet() {
		try {
			Date d = new Date();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("当前时间：");
			SimpleDateFormat formatter = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			stringBuilder.append(formatter.format(d));
			pipeline.set("ha666_time", stringBuilder.toString());
			pipeline.sync();
			System.out.println(jedis.get("ha666_time"));
		} catch (Exception e) {
			logger.error(String.format("ex:{0}", e));
			System.err.print(e);
		}
	}

}
