﻿namespace Ha666.Redis
{
    internal class RedisInfoType
    {
        internal const string Server = "server";
        internal const string Clients = "clients";
        internal const string Memory = "memory";
        internal const string Persistence = "persistence";
        internal const string Stats = "stats";
        internal const string Replication = "replication";
        internal const string Cpu = "cpu";
        internal const string CommandStats = "commandstats";
        internal const string Cluster = "cluster";
        internal const string KeySpace = "keyspace";
        internal const string All = "all";
        internal const string Default = "default";
    }

}
