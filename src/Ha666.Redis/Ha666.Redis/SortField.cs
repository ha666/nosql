﻿using ProtoBuf;

namespace Ha666.Redis
{
    public class SortField
    {
        public long Name
        {
            get;
            set;
        }
        public string Value
        {
            get;
            set;
        }
    }
}
