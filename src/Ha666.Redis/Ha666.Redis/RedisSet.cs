﻿using System.Collections.Generic;

namespace Ha666.Redis
{
    public abstract class RedisSet<T>
    {

        public RedisSet(string key, DataType dataType)
        {
            mDataType = dataType;
            mDataKey = key;
        }

        private string mDataKey;

        private DataType mDataType;

        public int Sadd<T>(int serverid, T member)
        {
            return Sadds<T>(serverid, new List<T>() { member });
        }

        public int Sadds<T>(int serverid, List<T> members)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.Sadd<T>((string)mDataKey, members, mDataType, serverid);
        }

        public int Scard(int serverid)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.Scard((string)mDataKey, serverid);
        }

        public bool Sismember<T>(int serverid, T member)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.Sismember<T>((string)mDataKey, member, mDataType, serverid);
        }

        public List<T> Smember<T>(int serverid)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.Smember<T>((string)mDataKey, mDataType, serverid);
        }

        public int Srem<T>(int serverid, T member)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.Srem<T>((string)mDataKey, member, mDataType, serverid);
        }

    }

    public class StringSet : RedisSet<string>
    {
        public StringSet(string key)
            : base(key, DataType.String)
        {
        }
    }

    public class ProtobufSet<T> : RedisSet<T>
    {
        public ProtobufSet(string key)
            : base(key, DataType.Protobuf)
        {
        }
    }

}
