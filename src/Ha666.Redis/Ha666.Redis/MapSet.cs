﻿using System.Collections.Generic;

namespace Ha666.Redis
{
    public abstract class MapSet
    {
        public MapSet(string key, DataType dataType)
        {
            mDataKey = key;
            mDataType = dataType;
        }

        private string mDataKey;

        private DataType mDataType;

        public IList<T> MGet<T>(int serverid, string[] fields)
        {
            NameType[] nts = new NameType[fields.Length];
            for (int i = 0; i < fields.Length; i++)
            {
                nts[i] = new NameType(fields[i], i);
            }
            RedisClient db = RedisClient.GetClient(null);
            return db.GetFields<T>(mDataKey, nts, mDataType, serverid);
        }

        public bool HExists(int serverid, string ke)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.HExists(mDataKey, ke, mDataType, serverid) > 0;
        }

        public IList<string> Keys(int serverid)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.GetKeys(mDataKey, mDataType, serverid);
        }

        public List<T> Vals<T>(int serverid)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.GetVals<T>(mDataKey, mDataType, serverid);
        }

        public T Hget<T>(int serverid, string name)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.Hget<T>(mDataKey, name, mDataType, serverid);
        }

        public List<Field> GetAll<T>(int serverid)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.GetAlls<T>(mDataKey, mDataType, serverid);
        }

        public bool MSet(int serverid, string name, object value)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.SetFields((string)mDataKey, name, value, mDataType, serverid);
        }

        public int HSet(int serverid, string name, object value)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.HSet((string)mDataKey, name, value, mDataType, serverid);
        }

        public void Setnx(int serverid, Field item)
        {
            RedisClient db = RedisClient.GetClient(null);
            db.Setnx((string)mDataKey, item, mDataType, serverid);
        }

        public int HLen(int serverid)
        {
            RedisClient db = RedisClient.GetClient(null);
            return db.HLEN((string)mDataKey, serverid);
        }

        public void Clear(int serverid)
        {
            RedisClient db = RedisClient.GetClient(null);
            db.Delete(serverid, mDataKey);
        }

        public void Remove(int serverid, string field)
        {
            RedisClient db = RedisClient.GetClient(null);
            db.HDEL(mDataKey, field, serverid);
        }

    }

    public class StringMapSet : MapSet
    {
        public StringMapSet(string key)
            : base(key, DataType.String)
        {
        }
    }

    public class ProtobufMapSet : MapSet
    {
        public ProtobufMapSet(string key)
            : base(key, DataType.Protobuf)
        {
        }
    }
}
