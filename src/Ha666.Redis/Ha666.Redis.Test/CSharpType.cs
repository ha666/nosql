﻿using ProtoBuf;

namespace Ha666.Redis.Test
{

    [ProtoContract]
    public class CSharpType
    {

        [ProtoMember(1)]
        public int Int0 { get; set; }

        [ProtoMember(2)]
        public int Int1 { get; set; }

        [ProtoMember(3)]
        public long Long0 { get; set; }

        [ProtoMember(4)]
        public long Long1 { get; set; }

        [ProtoMember(5)]
        public string String0 { get; set; }

        [ProtoMember(6)]
        public string String1 { get; set; }

    }
}
